import hashlib
import sqlite3
from datetime import datetime

def Creation(address,password :int):
    '''
    Création des utilisateurs 
    '''
    sql_create_user = '''
    INSERT INTO compte(
        id,
        montant
    ) VALUES (
        NULL,
        69
    );

    INSERT INTO user(
        id,
        address,
        password,
        fk_compte
    ) VALUES (
        NULL,
        "{0}",
        "{1}",
        (SELECT id 
        FROM compte
        ORDER BY id DESC
        LIMIT 1)
    );
    '''.format(address, hashlib.sha224(password.encode()).hexdigest())

    db = sqlite3.connect('bdd.sqlite')
    cursor = db.cursor()
    cursor.executescript(sql_create_user)
    db.commit()
    db.close()
    # try catch ? 
    return "Done"

def check_transfert(emetteur :str, montant :int):
    # Requete SQL qui donne le montant dans le compte de l'utilisateur
    sql_emetteur = '''
    SELECT c.montant
    FROM user AS u
    JOIN compte AS c
    ON u.fk_compte = c.id
    WHERE u.address = "{0}";
    '''.format(emetteur)

    # Execute la requête écrite au dessus
    db = sqlite3.connect('bdd.sqlite')
    cursor_emetteur = db.execute(sql_emetteur)
    compte_emetteur = ''
    for row in cursor_emetteur:
        compte_emetteur = row[0]
    db.close()

    # Compare ce qu'il y a dans le compte avec le montant de la transaction indiqué
    if compte_emetteur < montant:
        return False
    else:
        return True

def transfert(emetteur :str, recepteur :str, montant :int):
    sql_transfert = '''
    INSERT INTO transac (
        id,
        fk_compte_emetteur,
        fk_compte_recepteur,
        date_transaction,
        montant
    ) VALUES (
        NULL,
        (SELECT id FROM user WHERE address = "{0}"),
        (SELECT id FROM user WHERE address = "{1}"),
        "{2}",
        {3}
    );

    UPDATE compte 
    SET montant = (SELECT montant
    FROM compte
    WHERE id = (SELECT fk_compte FROM user WHERE address = "{0}")) - {3}
    WHERE id = (SELECT fk_compte FROM user WHERE address = "{0}");

    UPDATE compte 
    SET montant = (SELECT montant
    FROM compte
    WHERE id = (SELECT fk_compte FROM user WHERE address = "{1}")) + {3}
    WHERE id = (SELECT fk_compte FROM user WHERE address = "{1}");
    '''.format(emetteur, recepteur, datetime.now(), montant)

    db = sqlite3.connect('bdd.sqlite')
    cursor = db.cursor()
    cursor.executescript(sql_transfert)
    db.commit()
    db.close()
    # try catch ? 
    return "Done"

def afficher_compte(address :str, password :str):
    sql_select_user = '''
    SELECT u.address, c.montant
    FROM user AS u
    JOIN compte AS c
    ON u.fk_compte = c.id
    WHERE u.address = "{0}"
    AND u.password = "{1}";
    '''.format(address, hashlib.sha224(password.encode()).hexdigest())

    db = sqlite3.connect('bdd.sqlite')
    cursor = db.execute(sql_select_user)
    res_obj = {}
    for row in cursor:
        res_obj["address"] = row[0]
        res_obj["nb_bonheur"] = row[1]
    db.close()
    # try catch ? 
    return res_obj

def Delete(address :str, password :str):
    sql_delete_compte = '''
    UPDATE compte
    SET montant = 0
    WHERE id = (SELECT fk_compte FROM user WHERE address = "{0}");
    '''.format(address)

    sql_delete_user = '''
    DELETE FROM user WHERE address = "{0}" AND password = "{1}";
    '''.format(address, hashlib.sha224(password.encode()).hexdigest())

    db = sqlite3.connect('bdd.sqlite')
    cursor = db.cursor()
    cursor.executescript(sql_delete_compte)
    db.commit()
    cursor.executescript(sql_delete_user)
    db.commit()
    db.close()

    return "Done"
