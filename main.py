from typing import Optional
from pydantic import BaseModel
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from transacs import Creation,afficher_compte, transfert,Delete

app = FastAPI()


@app.post("/User/Create")
def createUser(email : str,password : str):
    return  Creation(email,password)

@app.post("/User/Transfert")
def Transfert(emetteur , recepteur , montant):
    return transfert(emetteur,recepteur,montant)

@app.get("/User/CheckAccount")
def CheckAccount(email,password):
    return afficher_compte(email,password)

@app.delete('/User/DeleteAccount')
def DeleteAccoun(email,password):
    return Delete(email,password)