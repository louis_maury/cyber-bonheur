import sqlite3

class DB():
    def __init__(self):
        self.connect = sqlite3.connect('./bdd.sqlite')
        self.request = ''

    def insert(self, table :str, list_columns :list, list_values :list):
        str_columns = ", ".join(list_columns)
        str_values = ", ".join(list_values)
        self.request = 'INSERT INTO {0} ({1}) VALUES ({2})'.format(table, str_columns, str_values)

    def select(self, table, values :list):
        str_vals = values
        self.request = ''

    def run(self):
        self.connect.execute(self.request)